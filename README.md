# jgreenemi-bitbucket-themes

Bitbucket themes intended for use with the Stylus plugin.

![Example screenshot for Solarized CSS](https://bitbucket.org/jgreenemi/jgreenemi-bitbucket-themes/raw/3848832a3ecbe288a89ac6b86caebaf254947852/resources/screenshot-Solarized.PNG)

## To Use

* Install Stylus or another similar user-style plugin for your browser.
* Create a new user style in the plugin. (Something like: "Write New Style")
* Paste in the appropriate CSS from this repository.
* Set up the "Applies To" rules to only apply to the `bitbucket.org` domain.
* Save and be on your way!
